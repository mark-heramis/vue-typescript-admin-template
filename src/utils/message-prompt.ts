import { Message } from 'element-ui'

const messagepromt = (message: any, type: any) => {
  Message({
    message: message,
    type: type,
    showClose: true
  })
}

export default messagepromt
