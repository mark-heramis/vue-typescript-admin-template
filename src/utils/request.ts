import axios from 'axios'
import messagepromt from '@/utils/message-prompt'
import { UserModule } from '@/store/modules/user'

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  timeout: Number(process.env.VUE_APP_TIMEOUT) // default request timeout wait time
  // withCredentials: true // send cookies when cross-domain requests
})

// Request interceptors
service.interceptors.request.use((config) => {
  // Add X-Access-Token header to every request, you can add other custom headers here
  if (UserModule.token) {
    config.headers['X-Access-Token'] = UserModule.token
    config.headers.Authorization = `Bearer ${UserModule.token}`
  }
  return config
}, (error) => {
  Promise.reject(error)
})

// Response interceptors
service.interceptors.response.use(
  (response) => {
    if (response.status !== 200) {
      if (response.data.error) {
        UserModule.ResetToken()
        location.reload()
      }
      messagepromt('Response error! Please contact developers!', 'error')
      return Promise.reject(new Error(response.data.error || 'Error'))
    } else {
      return response.data
    }
  },
  (error) => {
    if (error.response.status === 401) {
      messagepromt('Invalid Username or Password', 'error')
    } else {
      messagepromt('There was an unexpected error. Please contact developers!', 'error')
      return Promise.reject(error)
    }
  }
)

export default service
