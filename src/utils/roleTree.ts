export const getResourceName = (objects: {}) => {
  const temp: string[] = []
  // iterate through objects and get resource name
  for (const object in objects) {
    // slice string up to the character before '.'
    const key: string = object.substring(0, object.indexOf('.'))
    // store resource name to temp object if not exists
    if (!temp.includes(key)) {
      temp.push(key)
    }
  }
  return temp
}

export const getResourceNameAction = (resources: string[], objects: any) => {
  const temp: any = []
  // iterate parsed resource name
  for (let i = 0; i < resources.length; i++) {
    temp.push({
      label: resources[i],
      id: resources[i] + resources[i],
      /* value: resources[i] + resources[i], */
      children: []
    })
    // iterate permissions object
    for (const object in objects) {
      // if resource name is found on each permission objects
      if (object.includes(resources[i])) {
        // get resource action name which is found after the dot character
        const action: string = object.substring(object.indexOf('.') + 1, object.length)
        // store actions to temporary container
        temp[i].children.push({
          label: action,
          id: resources[i] + action,
          value: objects[object]
        })
      }
    }
  }
  return temp
}

export const getActionId = (object: any) => {
  const temp = []
  for (const obj of object) {
    temp.push(obj.id)
    for (const c of obj.children) {
      temp.push(c.id)
    }
  }
  return temp
}

export const serializeResource = (object: any) => {
  const temp: any = {}
  // iterate parsed resource
  for (let i = 0; i < object.length; i++) {
    // get each resource actions
    // concatenate resource name and its actions via dot char as key,
    // assign their values and save to temp
    for (const ch of object[i].children) {
      temp[object[i].label + '.' + ch.label] = ch.value
    }
  }
  return temp
}
