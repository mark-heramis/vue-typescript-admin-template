import request from '@/utils/request'

export const getUsers = (params: any) =>
  request({
    url: '/user',
    method: 'get',
    params
  })

export const getUserInfo = (data: any) =>
  request({
    url: '/me',
    method: 'get',
    data
  })

export const getUserByName = (username: string) =>
  request({
    url: `/user/${username}`,
    method: 'get'
  })

export const updateUser = (id: number, data: any) =>
  request({
    url: `/user/${id}`,
    method: 'put',
    data
  })

export const updateUserRole = (id: number, data: any) =>
  request({
    url: `/user/${id}/role`,
    method: 'put',
    data
  })

export const deleteUser = (id: number) =>
  request({
    url: `/user/${id}`,
    method: 'delete'
  })

export const deleteUserRole = (id: number, data: any) =>
  request({
    url: `/user/${id}/role`,
    method: 'delete',
    data
  })

export const login = (data: any) =>
  request({
    url: '/login',
    method: 'post',
    data
  })

export const logout = () =>
  request({
    url: '/user/logout',
    method: 'post'
  })

export const register = (data: any) =>
  request({
    url: '/register',
    method: 'post',
    data
  })
