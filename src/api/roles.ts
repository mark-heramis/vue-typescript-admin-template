import request from '@/utils/request'

export const getRoles = (params: any) =>
  request({
    url: '/role',
    method: 'get',
    params
  })

export const createRole = (data: any) =>
  request({
    url: '/role',
    method: 'post',
    data
  })

export const updateRole = (id: number, data: any) =>
  request({
    url: `/role/${id}`,
    method: 'put',
    data
  })

export const deleteRole = (slug: string) =>
  request({
    url: `/role/${slug}`,
    method: 'delete'
  })

export const getRoutes = (params: any) =>
  request({
    url: '/routes',
    method: 'get',
    params
  })
